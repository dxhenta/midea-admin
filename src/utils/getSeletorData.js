import { categorySelect } from '@/utils/consts'

async function getSN8List () {
  const { data: { data, code } } = await
  this.$http.get(this.getApiUrl(`dataanalysis/common/getSN8List`, false))
  if (code === 0) {
    return data.map(item => ({
      label: item,
      value: item
    }))
  }
}
async function getProvinceList () {
  const { data: { data, code } } = await this.$http.get(this.getApiUrl('dataanalysis/common/getProvinceList', false))
  if (code === 0) {
    return data.map(item => ({
      label: item,
      value: item
    }))
  }
}
async function getCityListByProvince () {
  const { data: { data, code } } = await this.$http.get(this.getApiUrl('dataanalysis/common/getCityListByProvince',
    false), { params: { province_name: this.queryParameter.province } })
  if (code === 0) {
    return data.map(item => ({
      label: item,
      value: item
    }))
  }
}
function getCategory () {
  return categorySelect
}

export { getSN8List, getProvinceList, getCityListByProvince, getCategory }
