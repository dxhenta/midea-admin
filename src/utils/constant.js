/**
 * Created by sf
 * 2019/11/12
 */

const PRODUCT_CONTENT_RELATIVE_URL_PREFIX = 'midea/menu/product/content/'

const IMAGE_UPLOAD_URL = 'http://47.106.93.196:8200/cloud-menu/midea/menu/file/upload'

const MACHINE_STATE_MONITOR_TYPE = {
  CPU: 1,
  MEMORY: 2,
  DISK: 3,
  API: 4
}

const MACHINE_STATE_MINITOR_TIME_UNIT_TYPE = {
  Month: {
    key: 'mon',
    format: 'YYYY-MM'
  },
  Day: {
    key: 'day',
    format: 'YYYY-MM-DD'
  },
  Hour: {
    key: 'hour',
    format: 'YYYY-MM-DD HH'
  },
  Minute: {
    key: 'min',
    format: 'YYYY-MM-DD HH:mm'
  }
}
export { IMAGE_UPLOAD_URL, PRODUCT_CONTENT_RELATIVE_URL_PREFIX, MACHINE_STATE_MONITOR_TYPE, MACHINE_STATE_MINITOR_TIME_UNIT_TYPE }
