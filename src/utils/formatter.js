/**
   * 将数字分段显示，每三位用逗号隔开
   * @param {Number} num
   */
export function numCommaFormat (num) {
  const prefix = num < 0 ? '-' : ''
  num = Math.abs(num).toString()
  const arr = num.split('.')
  const prev = arr[0].split('').reverse()
  const newPrev = []

  for (let i = 0; i < prev.length; i++) {
    newPrev.unshift(prev[i])
    if (i % 3 === 2 && i < (prev.length - 1)) {
      newPrev.unshift(',')
    }
  }

  arr[0] = newPrev.join('')
  return prefix + arr.join('.')
}
