const MikeBannerType = {

  Recipe: 0,
  Collection: 1,
  Activity: 2
}

const RequestMethod = {
  GET: 1,
  POST: 2,
  HEAD: 3,
  PUT: 4,
  DELETE: 5,
  CONNECT: 6,
  OPTIONS: 7,
  TRACE: 8

}

const dateFormat = 'yyyy-MM-dd'
const dateFormatMonent = 'YYYY-MM-DD'

const categorySelect = [
  {
    label: '微波炉',
    value: '0xB0'
  },
  {
    label: '大烤箱',
    value: '0xB1'
  },
  {
    label: '蒸汽炉',
    value: '0xB2'
  },
  {
    label: '小烤箱',
    value: '0xB4'
  },
  {
    label: '微蒸烤',
    value: '0xBF'
  },
  {
    label: '蒸烤',
    value: '0x9B'
  }
]

export {
  dateFormat,
  dateFormatMonent,
  MikeBannerType,
  RequestMethod,
  categorySelect
}
