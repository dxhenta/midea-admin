class Product {
  constructor ({
    id,
    productTypeId,
    productName,
    productCode,
    productUrlId,
    electronicVersionId,
    hardwarePlatformId,
    iotModuleId,
    productTempId,
    productFunctionRelationId,
    productAttrRelationId,
    cloudMenuRelationId,
    productQrcodId,
    luaProtocolVersionId,
    status,
    createTime,
    updateTime,
    productTpye,
    productUrl,
    urlIp,
    urlPort,
    versionName
  }) {
    this.id = id
    this.productTypeId = productTypeId
    this.productName = productName
    this.productCode = productCode
    this.productUrlId = productUrlId
    this.electronicVersionId = electronicVersionId
    this.hardwarePlatformId = hardwarePlatformId
    this.iotModuleId = iotModuleId
    this.productTempId = productTempId
    this.productFunctionRelationId = productFunctionRelationId
    this.productAttrRelationId = productAttrRelationId
    this.cloudMenuRelationId = cloudMenuRelationId
    this.productQrcodId = productQrcodId
    this.luaProtocolVersionId = luaProtocolVersionId
    this.status = status
    this.createTime = createTime
    this.updateTime = updateTime
    this.productTpye = productTpye
    this.productUrl = productUrl
    this.urlIp = urlIp
    this.urlPort = urlPort
    this.versionName = versionName
    this.setProductImageInfo(this.urlIp, this.urlPort, this.productUrl)
  }

  setProductImageInfo (ip, port, url) {
    this.productImage = `http://${ip}:${port}/product_image/${url}`
  }
}

export default Product
