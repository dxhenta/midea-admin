import Cookies from 'js-cookie'
export default {
  data () {
    return {
      productTemplates: [],
      hardwarePlatforms: [],
      productionTypes: [],
      electVersions: [],
      iotModules: [],
      luaProtocolVersions: [],
      imgFiles: [],
      imgUploadPath:
        window.SITE_CONFIG['apiURL'] +
        '/product/pproductinfo/productimage/upload',
      imgUploadHeaders: {
        // eslint-disable-next-line no-undef
        token: Cookies.get('token')
      },
      form: {
        // cloudMenuRelationId: '', // 云菜谱id
        // createTime: '',
        electronicVersionId: '0', // 电控版本协议id
        hardwarePlatformId: '0', // 硬件平台id
        // id: 0,
        iotModuleId: '0', // 物联模组id
        luaProtocolVersionId: '0', // lua协议版本id
        // productUrl: '',
        productUrlId: '', // 图片Url Id
        // productAttrRelationId: '', // 产品属性id
        productCode: '', // 型号码
        // productFunctionRelationId: '', // 产品功能id
        productName: '', // 产品名称
        // productQrcodId: '', // 二维码id
        productTempId: '0', // 产品模板id
        // productTpye: '',
        productTypeId: '' // 产品类型id
        // status: 0,
        // updateTime: '',
        // urlHost: 0,
        // urlIp: '',
        // versionName: ''
      }
    }
  },
  methods: {
    /**
       * Request
       * */
    submit () {
      this.$refs['dataForm'].validate(valid => {
        if (!valid) {
          return false
        }
        if (this.requesting) {
          return
        }
        this.requesting = true

        this.$http[!this.form.id ? 'post' : 'put']('/product/pproductinfo', this.form)
          .then(({ data: res }) => {
            // console.log('after saving product info', res)
            if (res.code !== 0) {
              return this.$message.error(res.msg)
            }
            this.handleSubmitSuccess()
          })
          .finally(() => {
            this.requesting = false
          })
      })
    },

    handleSubmitSuccess () {
      // 可在组件中继承实现
    },

    /**
     * 图片上传
     * */
    onImgUploadSuccess (response, file, fileList) {
      if (response.code !== 0) {
        this.form.productUrlId = ''
      }
      this.form.productUrlId = response.data
    },
    onImgRemoved () {
      this.form.productUrlId = ''
    },

    // oImgPreview () {
    //   // this.previewImgUrl = this.form.url
    //   // this.previewDialogVisible = true
    // },

    handleImgFileExceed (files, fileList) {
      this.$message.warning(`只能设置一张图片哦`)
    },

    /**
     * 获取所有相关信息列表
     * 物联模组
     * Lua协议版本
     * 模板
     * 类型
     * 电控版本协议
     * 硬件平台
     * */

    getProductTypes () {
      return this.$http
        .get('/product/pproductinfo/producttypelist')
        .then(({ data: res }) => {
          if (!res.data) {
            this.productionTypes = []
            return
          }

          this.productionTypes = [
            {
              id: '0',
              label: '空',
              value: '0'
            },
            ...res.data.map(item => {
              return { id: item.id, label: item.productType, value: item.id }
            })
          ]
        })
    },
    getElectronicVersions () {
      return this.$http
        .get('/product/pproductinfo/electronicprotocollist')
        .then(({ data: res }) => {
          if (!res.data) {
            this.electVersions = []
            return
          }
          this.electVersions = [
            {
              id: '0',
              label: '空',
              value: '0'
            },
            ...res.data.map(item => {
              return { id: item.id, label: item.versionName, value: item.id }
            })
          ]
        })
    },
    getLuaProtocolVersions () {},
    getIOTModules () {
      return this.$http
        .get('/product/pproductinfo/iotmodulelist')
        .then(({ data: res }) => {
          if (!res.data) {
            this.iotModules = []
            return
          }
          this.iotModules = [
            {
              id: '0',
              label: '空',
              value: '0'
            },
            ...res.data.map(item => {
              return { id: item.id, label: item.moduleName, value: item.id }
            })
          ]
        })
    },
    getProductTemplates () {
      return this.$http
        .get('/product/pproductinfo/producttemplist')
        .then(({ data: res }) => {
          if (!res.data) {
            this.productTemplates = []
            return
          }
          this.productTemplates = [
            {
              id: '0',
              label: '空',
              value: '0'
            },
            ...res.data.map(item => {
              return { id: item.id, label: item.tempName, value: item.id }
            })
          ]
        })
    },
    getHardwarePlatForms () {
      return this.$http
        .get('/product/pproductinfo/hardwareplatformlist')
        .then(({ data: res }) => {
          if (!res.data) {
            this.hardwarePlatforms = []
            return
          }
          this.hardwarePlatforms = [
            {
              id: '0',
              label: '空',
              value: '0'
            },
            ...res.data.map(item => {
              return { id: item.id, label: item.hardwareName, value: item.id }
            })
          ]
        })
    },
    getProductCodes () {}
  }
}
