export default {
  data () {
    this.chart1Settings = {
      labelMap: {
        'parker': '智能设备总提货量',
        '设备激活数': '智能设备总激活数',
        '设备连接数': '智能设备总连接数',
        'app月活数': 'app月活数'
      },
      showLine: ['parker']
    }
    this.extend = {
      series: {
        label: { show: true, position: 'top' }
      }
    }
    this.chart2Settings = {
      labelMap: {
        'B0': 'B0设备激活数',
        'B1': 'B1设备激活数',
        'B2': 'B2设备激活数',
        'B4': 'B4设备激活数',
        'B8': 'B8设备激活数'
      }
    }
    this.chart3Settings = {
      labelMap: {
        'B0': 'B0设备连接数',
        'B1': 'B1设备连接数',
        'B2': 'B2设备连接数',
        'B4': 'B4设备连接数',
        'B8': 'B8设备连接数'
      }
    }
    return {
      searchCondition: {
        activityStatus: '0'
      },
      chartData1: {
        columns: ['月份', 'parker', '设备激活数', '设备连接数', 'app月活数'],
        rows: [
          { '月份': '1/12', 'parker': 7000, '设备激活数': 2000, '设备连接数': 700, 'app月活数': 2000 },
          { '月份': '2/12', 'parker': 9000, '设备激活数': 2800, '设备连接数': 900, 'app月活数': 2500 },
          { '月份': '3/12', 'parker': 10000, '设备激活数': 3888, '设备连接数': 899, 'app月活数': 3000 },
          { '月份': '4/12', 'parker': 18000, '设备激活数': 4999, '设备连接数': 1000, 'app月活数': 4000 },
          { '月份': '5/12', 'parker': 25000, '设备激活数': 5100, '设备连接数': 1300, 'app月活数': 5000 },
          { '月份': '6/12', 'parker': 36000, '设备激活数': 5550, '设备连接数': 1290, 'app月活数': 5300 },
          { '月份': '7/12', 'parker': 40000, '设备激活数': 6000, '设备连接数': 1600, 'app月活数': 600 },
          { '月份': '8/12', 'parker': 43000, '设备激活数': 5800, '设备连接数': 2000, 'app月活数': 6000 },
          { '月份': '9/12', 'parker': 50000, '设备激活数': 7000, '设备连接数': 3000, 'app月活数': 6100 },
          { '月份': '10/12', 'parker': 52000, '设备激活数': 8000, '设备连接数': 3990, 'app月活数': 8000 },
          { '月份': '11/12', 'parker': 56000, '设备激活数': 8500, '设备连接数': 5000, 'app月活数': 8000 },
          { '月份': '12/12', 'parker': 60000, '设备激活数': 10000, '设备连接数': 7000, 'app月活数': 9000 }
        ]
      },
      chartData2: {
        columns: ['月份', 'B0', 'B1', 'B2', 'B4', 'B8'],
        rows: [
          { '月份': '1/12', 'B0': 300, 'B1': 200, 'B2': 666, 'B4': 222, 'B8': 333 },
          { '月份': '2/12', 'B0': 350, 'B1': 323, 'B2': 333, 'B4': 222, 'B8': 333 },
          { '月份': '3/12', 'B0': 298, 'B1': 260, 'B2': 666, 'B4': 222, 'B8': 333 },
          { '月份': '4/12', 'B0': 190, 'B1': 322, 'B2': 222, 'B4': 222, 'B8': 333 },
          { '月份': '5/12', 'B0': 333, 'B1': 232, 'B2': 332, 'B4': 222, 'B8': 333 },
          { '月份': '6/12', 'B0': 455, 'B1': 500, 'B2': 666, 'B4': 222, 'B8': 333 },
          { '月份': '7/12', 'B0': 700, 'B1': 1093, 'B2': 1000, 'B4': 222, 'B8': 333 },
          { '月份': '8/12', 'B0': 1000, 'B1': 200, 'B2': 666, 'B4': 222, 'B8': 333 },
          { '月份': '9/12', 'B0': 2923, 'B1': 2623, 'B2': 666, 'B4': 222, 'B8': 333 },
          { '月份': '10/12', 'B0': 1723, 'B1': 1423, 'B2': 666, 'B4': 222, 'B8': 333 },
          { '月份': '11/12', 'B0': 3792, 'B1': 1000, 'B2': 666, 'B4': 222, 'B8': 333 },
          { '月份': '12/12', 'B0': 1393, 'B1': 1093, 'B2': 666, 'B4': 222, 'B8': 333 }
        ]
      },
      chartData3: {
        columns: ['月份', 'B0', 'B1', 'B2', 'B4', 'B8'],
        rows: [
          { '月份': '1/12', 'B0': 200, 'B1': 100, 'B2': 366, 'B4': 222, 'B8': 233 },
          { '月份': '2/12', 'B0': 350, 'B1': 203, 'B2': 333, 'B4': 300, 'B8': 333 },
          { '月份': '3/12', 'B0': 298, 'B1': 260, 'B2': 666, 'B4': 222, 'B8': 333 },
          { '月份': '4/12', 'B0': 190, 'B1': 322, 'B2': 222, 'B4': 222, 'B8': 400 },
          { '月份': '5/12', 'B0': 333, 'B1': 232, 'B2': 332, 'B4': 222, 'B8': 333 },
          { '月份': '6/12', 'B0': 555, 'B1': 570, 'B2': 666, 'B4': 400, 'B8': 333 },
          { '月份': '7/12', 'B0': 700, 'B1': 1093, 'B2': 1000, 'B4': 222, 'B8': 333 },
          { '月份': '8/12', 'B0': 1000, 'B1': 200, 'B2': 666, 'B4': 222, 'B8': 400 },
          { '月份': '9/12', 'B0': 2523, 'B1': 2623, 'B2': 666, 'B4': 500, 'B8': 500 },
          { '月份': '10/12', 'B0': 1723, 'B1': 1423, 'B2': 666, 'B4': 300, 'B8': 333 },
          { '月份': '11/12', 'B0': 3792, 'B1': 1000, 'B2': 666, 'B4': 222, 'B8': 550 },
          { '月份': '12/12', 'B0': 1393, 'B1': 1093, 'B2': 666, 'B4': 222, 'B8': 333 }
        ]
      }
    }
  },
  methods: {
    initDeviceData () {
      this.$http.post()
        .then(res => {
          this.loading = false
        })
        .catch(() => {
          this.loading = false
        })
    },
    yearChanged () {
      // this.searchCondition.sn8 = sn8
      let chartData1 = JSON.parse(JSON.stringify(this.chartData1))
      if (this.searchCondition.activityStatus === '1') {
        chartData1.rows[0]['设备提货量'] = '10000'
      } else {
        chartData1.rows[0]['设备提货量'] = '1300'
      }
      this.chartData1 = JSON.parse(JSON.stringify(chartData1))
      // this.initDeviceData()
    }
  }
}
