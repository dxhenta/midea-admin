import { commonMixin, subComponentMixin } from '../user-data'

commonMixin.methods.formatParams = (obj = {}) => {
  // eslint-disable-next-line camelcase
  const [start_day, end_day] = obj.dateRange || []
  const params = Object.assign({}, obj)
  // Reflect.deleteProperty(params, 'dateRange')
  const { province, category, model } = params
  return {
    province_name: province || undefined,
    apptype: category || undefined,
    product_model: model || undefined,
    start_day,
    end_day
  }
}

export { commonMixin, subComponentMixin }
