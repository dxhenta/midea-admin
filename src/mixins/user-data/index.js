import debounce from 'lodash/debounce'

const commonMixin = {
  computed: {
    requestParams () {
      return this.formatParams(this.queryParameter)
    }
  },
  methods: {
    getApiUrl (relativeUrl, mike = true) {
      const url = mike ? 'mike_apiURL' : 'apiURL'
      return window.SITE_CONFIG[url] + relativeUrl
    },
    formatParams (obj = {}) {
      // eslint-disable-next-line camelcase
      const [start_day, end_day] = obj.dateRange || []
      const { province, city, category, model } = obj
      return {
        province_name: province || undefined,
        city_name: city || undefined,
        apptype: category || undefined,
        sn8: model || undefined,
        start_day,
        end_day
      }
    }
  }
}
const subComponentMixin = {
  props: {
    queryData: {
      type: Object,
      required: true
    }
  },
  data () {
    return {
      debounceGetData: debounce(this.getData || (() => console.error('this.getData is not a function')), 1000, { leading: true })
    }
  },
  computed: {
    requestParams () {
      return this.formatParams(this.queryData)
    }
  },
  watch: {
    queryData: {
      handler () {
        (typeof this.debounceGetData) === 'function' && this.debounceGetData()
      },
      deep: true
    }
  },
  methods: Object.assign({}, commonMixin.methods)
}
export { commonMixin, subComponentMixin }
