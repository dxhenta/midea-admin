/**
 * Created by sf
 * 2019/11/11
 */

let commonMixin = {
  data () {
    return {
      loading: false
    }
  },
  methods: {
    getApiUrl (relativeUrl, mike = true) {
      const url = mike ? 'mike_apiURL' : 'apiURL'
      return window.SITE_CONFIG[url] + relativeUrl
    },
    sn8Changed (sn8) {
      this.dataForm.sn8 = sn8
    },
    dateDataBol (property, bol) {
      return function (a, b) {
        var value1 = a[property]
        var value2 = b[property]
        if (bol) {
          return Date.parse(value1) - Date.parse(value2)
        } else {
          return Date.parse(value2) - Date.parse(value1)
        }
      }
    }
  }
}

export default commonMixin
