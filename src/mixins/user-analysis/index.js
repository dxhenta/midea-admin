import _ from 'lodash'
import moment from 'moment'
import TableSimple from '@/views/modules/user-data/table-simple'
import { dateFormat, dateFormatMonent } from '@/utils/consts'

const commonMixin = {
  components: { TableSimple },
  data () {
    return {
      dateFormat,
      dateRange: '1',
      dateOption: [
        {
          label: '当天',
          value: '1'
        },
        {
          label: '近7天',
          value: '7'
        },
        {
          label: '近14天',
          value: '14'
        },
        {
          label: '近30天',
          value: '30'
        },
        {
          label: '自定义',
          value: 'custom'
        }
      ]
    }
  },
  computed: {
    showDateRangePicker () {
      return this.dateRange === _.last(this.dateOption).value
    }
  },
  created () {
    // 接口调用入口 触发watcher created > watcher
    !(/\D/g.test(this.dateRange)) && (this.queryParameter.dateRange = [moment().add(`-${this.dateRange}`, 'days').format(dateFormatMonent), moment(new Date()).format(dateFormatMonent)])
  },
  methods: {
    handleDateChange () {
      switch (this.dateRange) {
        case '':
          this.queryParameter.dateRange = null
          break
        case '1':
          this.queryParameter.dateRange = [
            moment(new Date()).format(dateFormatMonent),
            moment(new Date()).format(dateFormatMonent)
          ]
          break
        case '7':
          this.queryParameter.dateRange = [
            moment()
              .add('-7', 'days')
              .format(dateFormatMonent),
            moment(new Date()).format(dateFormatMonent)
          ]
          break
        case '14':
          this.queryParameter.dateRange = [
            moment()
              .add('-14', 'days')
              .format(dateFormatMonent),
            moment(new Date()).format(dateFormatMonent)
          ]
          break
        case '30':
          this.queryParameter.dateRange = [
            moment()
              .add('-30', 'days')
              .format(dateFormatMonent),
            moment(new Date()).format(dateFormatMonent)
          ]
          break
        default:
          break
      }
    }
  }
}

export { commonMixin }
