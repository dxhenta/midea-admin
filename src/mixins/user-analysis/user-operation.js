import { commonMixin, subComponentMixin } from '../user-data'

Object.assign(subComponentMixin.methods, {
  formatParams (obj = {}) {
    // eslint-disable-next-line camelcase
    const [start_day, end_day] = this.queryParameter.dateRange || []
    const { province, city, category, model } = obj
    return {
      province_name: province || undefined,
      city_name: city || undefined,
      apptype: category || undefined,
      sn8: model || undefined,
      start_day,
      end_day
    }
  }
})

Object.assign(subComponentMixin.computed, {
  requestParams () {
    return this.formatParams(this.queryData)
  }
})
Object.assign(subComponentMixin.watch, {
  'queryParameter.dateRange': {
    handler () {
      this.debounceGetData()
    }
  }
})

export { commonMixin, subComponentMixin }
