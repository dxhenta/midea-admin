export default {
  computed: {
    chartExtend () {
      /* eslint-disable */
      console.log(this.abc)
      return {
        title: {
          show: true,
          text: this.subtitle,
          left: 'center',
          top: '120px',
          textStyle: {
            color: '#fff'
          }
        },
        series: {
          // color: '#65d186',
          barWidth: 30,
          label: { show: true, position: 'top', color: 'rgba(255,255,255, 1)' }
        },
        legend: { // 图标上方的title样式
          show: false,
          textStyle: {
            color: 'white',
            fontSize: 18
          }
        }
      }
      /* eslint-enable */
    }
  }
}
