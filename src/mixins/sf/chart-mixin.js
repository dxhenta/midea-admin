const COLORS = ['#19d4ae', '#5ab1ef',
  '#0067a6', '#c4b4e4',
  '#d87a80', '#9cbbff',
  '#87a997', '#d49ea2',
  '#7ba3a8']

export default {
  data () {
    /* eslint-disable */
    return {
      chartExtend: {
        series: {
          // color: '#65d186',
          // barWidth: 30,
          label: { show: true, position: 'top', color: 'rgba(255,255,255, 1)', formatter: function (params) {
              // let value = (params.data instanceof Array) ? params.data[1] : params.data;
              // console.log(params)
              // return `${( value * 100).toFixed(2)}%`
              return `${params.name}:00`
            } }
        },
        legend: { // 图标上方的title样式
          show: false,
          textStyle: {
            color: 'white',
            fontSize: 18
          }
        },
        xAxis: {
          show: true,
          axisLine: {
            show: true,
            lineStyle: {
              color: 'rgba(255,255,255, 1)'
            }
          },
          axisLabel: {
            show: false,
            textStyle: {
              color: 'rgba(255,255,255, 1)'
            },
            interval: 0
          },
          axisTick: { // 隐藏坐标轴刻度
            show: false
          }
        },
        yAxis: {
          splitLine: false,
          axisLine: {
            show: true,
            lineStyle: {
              color: 'rgba(255,255,255, 1)'
            }
          },
          // axisLabel: { // 该项设置影响到纵坐标百分比值格式化
          //   textStyle: {
          //     color: 'rgba(255,255,255, 1)'
          //   }
          // }
        }
      }
    }
    /* eslint-enable */
  },
  methods: {
    getRandomNum () {
      return Math.floor(Math.random() * 10)
    },
    getRandomColor () {
      return COLORS[this.getRandomNum()]
    }
  }
}
