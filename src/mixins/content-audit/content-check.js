const requestNetworkBase = 'http://47.106.93.196:8200/cloud-menu/midea/menu/confirm/get/allcontents'
export default {
  data () {
    return {
      loading: false,
      searchCondition: {
        activityName: '',
        activityStatus: '',
        beginDate: '',
        beginDateShow: '',
        endDateShow: '',
        endDate: ''
      },
      tablePage: {
        currentPage: 1,
        pageSize: 10,
        total: 0
      },
      // 表头信息
      theadListDefault: [
        {
          label: '名称',
          prop: 'name',
          width: 200
        },
        {
          label: '分类',
          prop: 'type',
          width: 200
        },
        {
          label: '时间',
          prop: 'createTime',
          width: 200
        },
        // 0审核通过 1待审核 2驳回
        {
          label: '状态',
          prop: 'state',
          width: 200
        }
      ],
      // 表格数据
      tableMsg: [],
      operateOptions: [
        {
          label: '详情',
          show: '0,1,2', // 展示范围
          active: '0,1,2', // 高亮展示范围
          key: 'edit'
        },
        {
          label: '通过',
          show: '0,1,2',
          active: '1',
          key: 'check'
        },
        {
          label: '驳回',
          show: '0,1,2',
          active: '2',
          key: 'release'
        }
      ]
    }
  },
  methods: {
    /**
     * 获取日期选择组件disabledDate函数
     * @param  {Date/String} limitDate 限制日期
     * @return {Function}       disabledDate函数
    */
    getdisableDateOption (limitDate) {
      if (!limitDate) return null
      if (limitDate instanceof Date) limitDate = limitDate.getTime()
      return time => time.getTime() <= limitDate
    },
    // 重置
    resetForm () {
      this.$refs.activityView.resetFields()
      this.$nextTick(() => this.initTableData())
    },
    // pagination ----start
    initTableData (type = 'condition') {
      let searchCondition = {}
      if (type === 'condition') {
        searchCondition = this.searchCondition
      }
      let params = Object.assign(
        {
          pageSize: this.tablePage.pageSize,
          pageIndex: this.tablePage.currentPage
        },
        searchCondition)
      this.$http.post(requestNetworkBase, { params: params })
        .then(res => {
          this.tableMsg = res.data.data
          // this.transformData()
          this.tablePage.total = res.data.total
          this.loading = false
        })
        .catch(() => {
          this.loading = false
        })
    },
    handleSizeChange (val) {
      this.tablePage.pageSize = val
      this.loading = true
      // 避免直接传currentPage导致多次请求
      if (this.tablePage.currentPage === 1) {
        this.initTableData()
      } else {
        // size-change中改变currentPage会自动触发current-change
        this.tablePage.currentPage = 1
      }
    },
    handleCurrentChange (val) {
      this.tablePage.currentPage = val
      this.loading = true
      this.initTableData()
    },
    handleSearch () {
      if (this.loading) return // 阻止enter键反复请求
      this.tablePage.currentPage = 1
      this.loading = true
      this.isShowPage = false
      this.initTableData()
    },
    // 活动管理操作
    async handleDetail (scope, type, auth) {
    }
    // pagination ----end
  }
}
