export default {
  data () {
    return {
      loading: false,
      searchCondition: {
        flag: '3',
        sn8: ''
      },
      tablePage: {
        currentPage: 1,
        pageSize: 10,
        total: 0
      },
      requestNetworkBase: window.SITE_CONFIG['mike_apiURL'],
      // 表头信息
      theadListDefault: [
        {
          label: '用户名',
          prop: 'name',
          width: 200
        },
        {
          label: '分类',
          prop: 'type',
          width: 200
        },
        {
          label: '时间',
          prop: 'createTime',
          width: 200
        },
        // 0审核通过 1待审核 2驳回
        {
          label: '状态',
          prop: 'state',
          width: 200
        }
      ],
      // 表格数据
      tableMsg: [],
      operateOptions: [
        {
          label: '详情',
          show: '0,1,2', // 展示范围
          active: '0,1,2', // 高亮展示范围
          key: 'edit',
          plain: true,
          type: 'primary'
        },
        {
          label: '通过',
          show: '0,1,2',
          active: '1',
          key: 'check',
          plain: false,
          type: 'success'
        },
        {
          label: '驳回',
          show: '0,1,2',
          active: '1',
          key: 'release',
          plain: false,
          type: 'danger'
        }
      ],
      dialogFormVisible: false,
      dialogItem: {}
    }
  },
  methods: {
    /**
     * 获取日期选择组件disabledDate函数
     * @param  {Date/String} limitDate 限制日期
     * @return {Function}       disabledDate函数
    */
    getdisableDateOption (limitDate) {
      if (!limitDate) return null
      if (limitDate instanceof Date) limitDate = limitDate.getTime()
      return time => time.getTime() <= limitDate
    },
    // 重置
    resetForm () {
      this.$refs.activityView.resetFields()
      this.$nextTick(() => this.initTableData())
    },
    // pagination ----start
    initTableData (type = 'condition') {
      let searchCondition = {}
      if (type === 'condition') {
        searchCondition = this.searchCondition
        // if (!searchCondition.sn8) {
        //   delete searchCondition.sn8
        // }
        if (!searchCondition.flag) {
          searchCondition.flag = '3'
        }
      }
      let params = {
        pageSize: this.tablePage.pageSize,
        currentPage: this.tablePage.currentPage,
        flag: this.searchCondition.flag ? parseInt(this.searchCondition.flag) : 3,
        sn8: this.searchCondition.sn8
      }
      let requestUrl = this.requestNetworkBase + 'midea/menu/confirm/get/pics'
      this.$http.post(requestUrl, params)
        .then(res => {
          this.tableMsg = res.data.data
          // this.transformData()
          this.tablePage.total = res.data.totalCounts
          this.loading = false
        })
        .catch(() => {
          this.loading = false
        })
    },
    handleSizeChange (val) {
      this.tablePage.pageSize = val
      this.loading = true
      // 避免直接传currentPage导致多次请求
      if (this.tablePage.currentPage === 1) {
        this.initTableData()
      } else {
        // size-change中改变currentPage会自动触发current-change
        this.tablePage.currentPage = 1
      }
    },
    handleCurrentChange (val) {
      this.tablePage.currentPage = val
      this.loading = true
      this.initTableData()
    },

    handleFlagChange () {
      this.handleSearch()
    },
    handleSearch () {
      if (this.loading) return // 阻止enter键反复请求
      this.tablePage.currentPage = 1
      this.loading = true
      this.isShowPage = false
      this.initTableData()
    },
    // 活动管理操作
    handleDetail (scope, type, auth) {
      let params = {
      }
      let requestUrl = ''
      if (type === 'edit') {
        this.dialogFormVisible = true
        this.dialogItem = scope.row
        this.dialogItem.desc = this.dialogItem.description ? this.dialogItem.description : '无'
        this.dialogItem.flag = this.dialogItem.flag ? this.dialogItem.flag : 0
        switch (parseInt(this.dialogItem.flag)) {
          case 0:
            this.dialogItem.flagStatus = '审核通过'
            break
          case 1:
            this.dialogItem.flagStatus = '待审核'
            break
          case 2:
            this.dialogItem.flagStatus = '驳回'
            break
          default:
            break
        }
        return
      }
      if (type === 'check') {
        requestUrl = this.requestNetworkBase + 'midea/menu/confirm/pic/' + scope.row.id
      }
      if (type === 'release') {
        let reason = 'reject'
        requestUrl = this.requestNetworkBase + 'midea/menu/reject/pic/' + scope.row.id + '/' + reason
      }
      if (this.loading) {
        return
      }
      this.loading = true

      this.$http.post(requestUrl, params)
        .then(res => {
          this.loading = false
          this.initTableData()
          this.$message({
            message: this.$t('prompt.success'),
            type: 'success',
            duration: 500
            // onClose: () => {
            //   this.getDataList()
            // }
          })
        })
        .catch(() => {
          this.loading = false
        })
    },
    // pagination ----end

    dialogHandle (type) {
      this.dialogFormVisible = false
    },
    sn8Changed (sn8) {
      // this.searchCondition.sn8 = sn8
      this.handleSearch()
    }
  }
}
