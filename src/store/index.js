import Vue from 'vue'
import Vuex from 'vuex'
import cloneDeep from 'lodash/cloneDeep'

Vue.use(Vuex)

// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = require.context('./modules', true, /\.js$/)

// you do not need `import app from './modules/app'`
// it will auto require all vuex module from modules file
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  // set './app.js' => 'app'
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

export default new Vuex.Store({
  namespaced: true,
  state: {
    // 全屏状态
    screenfull: false,
    // 导航条, 布局风格, defalut(白色) / colorful(鲜艳)
    navbarLayoutType: 'colorful',
    // 侧边栏, 布局皮肤, default(白色) / dark(黑色)
    sidebarLayoutSkin: 'dark',
    // 侧边栏, 折叠状态
    sidebarFold: false,
    // 侧边栏, 菜单
    sidebarMenuList: [],
    sidebarMenuActiveName: '',
    // 内容, 是否需要刷新
    contentIsNeedRefresh: false,
    // 内容, 标签页(默认添加首页)
    contentTabs: [
      {
        ...window.SITE_CONFIG['contentTabDefault'],
        name: 'home',
        title: 'home'
      }
    ],
    contentTabsActiveName: 'home'
  },
  modules,
  mutations: {
    // 重置vuex本地储存状态
    resetStore (state) {
      Object.keys(state).forEach(key => {
        state[key] = cloneDeep(window.SITE_CONFIG['storeState'][key])
      })
    }
    // 删除Tab
    // tabRemove (state, { tabName }) {
    //   if (tabName === 'home') {
    //     return false
    //   }
    //   state.contentTabs = state.contentTabs.filter(
    //     item => item.name !== tabName
    //   )
    //   if (state.contentTabs.length <= 0) {
    //     state.sidebarMenuActiveName = state.contentTabsActiveName = 'home'
    //     return false
    //   }
    //   // 当前选中tab被删除
    //   if (tabName === state.contentTabsActiveName) {
    //     this.$router.push({
    //       name: state.contentTabs[state.contentTabs.length - 1].name
    //     })
    //   }
    // }
  }
})
