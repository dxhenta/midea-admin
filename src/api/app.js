
export function getUserData (data) {
  return this.$http.post('/cloud-menu/midea/menu/analyse/usercounts', data)
}

export function dayWeekMonthUser (data, type = 'add') {
  return this.$http.post(`/cloud-menu/midea/menu/analyse/dayweekmonth${type}user`, data)
}
