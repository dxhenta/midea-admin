import Vue from 'vue'
import Element from 'element-ui'
import App from '@/App'
import i18n from '@/i18n'
import router from '@/router'
import store from '@/store'
import '@/icons'
import '@/element-ui/theme/index.css'
import '@/assets/scss/aui.scss'
import http from '@/utils/request'
import { hasPermission } from '@/utils'
import cloneDeep from 'lodash/cloneDeep'
import MideaAdminImage from '@/components/mike/mideaAdminImage'
import dataV from '@jiaminghi/data-view'
import VCharts from 'v-charts'
import moment from 'moment'
import _ from 'lodash'

Vue.use(dataV)
Vue.use(VCharts)

Vue.config.productionTip = false
Vue.component(MideaAdminImage.name, MideaAdminImage)
Vue.use(Element, {
  size: 'default',
  i18n: (key, value) => i18n.t(key, value)
})

// 挂载全局
Vue.prototype.$http = http
Vue.prototype._ = _
Vue.prototype.$hasPermission = hasPermission

// 保存整站vuex本地储存初始状态
window.SITE_CONFIG['storeState'] = cloneDeep(store.state)

new Vue({
  i18n,
  router,
  store,
  render: h => h(App),
  created () {
    moment.locale('zh-cn')
  }
}).$mount('#app')
